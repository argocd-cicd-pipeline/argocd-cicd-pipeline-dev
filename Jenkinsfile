node {
    def GITLAB_CODE_BRANCH = "${GITLAB_BRANCH}"
    def GITLAB_CODE_REPOSITORY_URL = "http://${GITLAB_DOMAIN}/argocd-cicd/argocd-cicd.git"
    def GITLAB_CODE_REPOSITORY_DIR = "argocd-cicd"

    def GITLAB_CICD_REPOSITORY_URL = "http://${GITLAB_DOMAIN}/argocd-cicd-pipeline/argocd-cicd-pipeline-dev.git"
    def GITLAB_CICD_REPOSITORY_DIR = "argocd-cicd-pipeline-dev"
    def GITLAB_CICD_REPOSITORY_CONTEXT_DIR = "dev"
    def GITLAB_CREDENTIAL_ID = "gitlab-root-credential"

    def SONARQUBE_INSTALLATION_NAME = "sonarqube-server"
    def SONARQUBE_PROJECT_KEY = GITLAB_CODE_REPOSITORY_DIR
    def SONARQUBE_CREDENTIAL_ID = "sonarqube-admin-credential"

    def NEXUS_VERSION = "nexus3"
    def NEXUS_PROTOCOL = "http"
    def NEXUS_URL = "${NEXUS_DOMAIN}:18084"
    def NEXUS_REPOSITORY = "${NEXUS_WTS_REPOSITORY}"
    def NEXUS_CREDENTIAL_ID = "nexus-admin-credential"
    def NEXUS_DOCKER_REGISTRY = "103.218.156.49:5000"

    def MAVEN_SETTINGS_FILE_ID = "maven-settings"
    def MAVEN_SERVER_ID = "maven-settings"

    def projectname = GITLAB_CICD_REPOSITORY_DIR.replace('-pipeline-dev', '')
    def DEPLOY_PATH = "/home/centos/"+projectname+"/"+GITLAB_CICD_REPOSITORY_CONTEXT_DIR

    def TARGET_VM_SSH_IPS = ["192.168.0.55"]
    def TARGET_VM_SSH_PORT = "22"
    def TARGET_VM_USER = "centos"
    def TARGET_VM_WAS_USER = "centos"
    def SSH_CREDENTIAL = "vm-ssh-credential"
    def TARGET_VM_LIST = []
    properties([
        parameters([
            gitParameter(name: 'Branch',
                        type: 'PT_BRANCH',
                        description: 'Select your branch.',
                        quickFilterEnabled: false,
                        branchFilter: 'origin/(.*)',
                        defaultValue: '',
                        selectedValue: 'NONE',
                        sortMode: 'DESCENDING_SMART',
                        listSize: '32',
                        useRepository: GITLAB_CODE_REPOSITORY_URL)
        ])
    ])

    dir(GITLAB_CODE_REPOSITORY_DIR) {
        stage('Pull Code From Git') {
            if("${params.Branch}"!="null"){
                GITLAB_CODE_BRANCH = "${params.Branch}"
            }
            git branch: GITLAB_CODE_BRANCH, url: GITLAB_CODE_REPOSITORY_URL, credentialsId: GITLAB_CREDENTIAL_ID
        }
        stage('Code Build') {
            withMaven(maven: 'maven3.6'){
                configFileProvider([configFile(fileId: MAVEN_SETTINGS_FILE_ID, variable: 'MAVEN_SETTINGS')]) {
                    sh '''
                        mvn clean package -U -DskipTests=true --settings $MAVEN_SETTINGS
                    '''
                }
            }
        }
        
    }
    //parallel 'Deploy Artifact To Nexus'
        dir(GITLAB_CODE_REPOSITORY_DIR) {
            stage('Deploy Artifact To Nexus') {
                withMaven(maven: 'maven3.6') {
                    configFileProvider([configFile(fileId: MAVEN_SETTINGS_FILE_ID, variable: 'MAVEN_SETTINGS')]) {
                        pom = readMavenPom file: "pom.xml";
                        version = pom.version!=null?pom.version:pom.parent.version
                        echo "*** group: ${pom.groupId}, packaging: ${pom.packaging}, version: ${pom.version} artifactId: ${pom.artifactId}";
                        if(version!=null){
                            if(!version.endsWith("-SNAPSHOT")){
                                NEXUS_REPOSITORY = NEXUS_REPOSITORY.replace('-snapshots','-release')
                            }
                            sh '''
                                mvn deploy -U -DskipTests=true --settings $MAVEN_SETTINGS \
                                    -DaltDeploymentRepository='''+MAVEN_SERVER_ID+'''::default::'''+NEXUS_PROTOCOL+'''://'''+NEXUS_URL+'''/repository/'''+NEXUS_REPOSITORY+'''/
                            '''
                        }
                    }
                }
            }
        }
    
    //parallel 'Copy Artifact To Git (dev) & Push': {
        stage('Copy Artifact To Git (dev) & Push') {
            withCredentials([usernamePassword(credentialsId: GITLAB_CREDENTIAL_ID, usernameVariable: 'CICD_USERNAME', passwordVariable: 'CICD_PASSWORD')]) {
                GITLAB_CICD_REPOSITORY_URL2 = GITLAB_CICD_REPOSITORY_URL.replace('://', '://${CICD_USERNAME}:${CICD_PASSWORD}@')
                if (!fileExists(GITLAB_CICD_REPOSITORY_DIR)) {
                    sh '''
                        git clone '''+GITLAB_CICD_REPOSITORY_URL2+'''
                    '''
                }
                dir(GITLAB_CICD_REPOSITORY_DIR) {
                    if(Integer.parseInt(sh(script: 'git branch | grep main | wc -l', returnStdout: true).trim())==0) {
                        sh '''
                            git pull origin main
                            git remote set-url origin '''+GITLAB_CICD_REPOSITORY_URL2+'''
                            git add .
                            git config --global user.name "Jenkins Automation Server"
                            git config --global user.email "${CICD_USERNAME}@example.com"
                            git commit -m "add README"
                            git push -u '''+GITLAB_CICD_REPOSITORY_URL2+''' main
                        '''
                    }
                }
                def codeTagInfo = ""
                def artifactPath = ""
                def artifactName = ""
                def staticPathList = []
                dir(GITLAB_CODE_REPOSITORY_DIR) {
                    codeTagInfo = sh(script: 'echo $(git rev-parse --abbrev-ref HEAD)#$(git rev-parse --short HEAD)', returnStdout: true).trim()
                    filesByGlob = findFiles(glob: "**/*.war")
                    artifactPath = filesByGlob[0].path
                    artifactName = filesByGlob[0].name
                    echo ">>>artifactPath: ${artifactPath} artifactName: ${artifactName}<<<"
                    staticPathList.add("src")
                }
                dir(GITLAB_CICD_REPOSITORY_DIR) {
                    webCopyCLI = "rm -rf "+GITLAB_CICD_REPOSITORY_CONTEXT_DIR+"/web/"
                    if(staticPathList.size()>0){
                        webCopyCLI += " && mkdir -p "+GITLAB_CICD_REPOSITORY_CONTEXT_DIR+"/web/"
                        for(staticPath in staticPathList){
                            webCopyCLI += " && cp -r ../"+GITLAB_CODE_REPOSITORY_DIR+"/"+staticPath+" "+GITLAB_CICD_REPOSITORY_CONTEXT_DIR+"/web/"
                        }
                    }
                    def folderName = "${env.JOB_NAME}".split('/').first()
                    def jenkinsTagInfo = "${folderName}#${BUILD_NUMBER}"
                    def cicdTagInfo = jenkinsTagInfo+"="+codeTagInfo+"="+GITLAB_CICD_REPOSITORY_CONTEXT_DIR
                    //////////////////TAG (include Time) - Definition
                    def timeTagInfo = sh(script: 'date "+%Y%m%d%H%M"', returnStdout: true).trim()
                    cicdTagInfo = jenkinsTagInfo+"="+codeTagInfo+"="+timeTagInfo+"="+GITLAB_CICD_REPOSITORY_CONTEXT_DIR
                    //////////////
                    def commitInfo = cicdTagInfo
                    
                    
                    def rtag = cicdTagInfo.replace('=', '-')
                    def rrtag = rtag.replace('#', '-')
                    def dockernewTag = NEXUS_DOCKER_REGISTRY+"/"+GITLAB_CODE_REPOSITORY_DIR+"/metacicd:"+rrtag
                    
                    sh '''
                        echo "$GITLAB_CODE_REPOSITORY_DIR"
                    '''
                    def oldname = 'test-deploy'
                    sh '''
                        dockerognTag=`grep "image:" '''+GITLAB_CICD_REPOSITORY_CONTEXT_DIR+'''/'''+GITLAB_CODE_REPOSITORY_DIR+'''.yaml | awk '{print $3}'`

                        git remote set-url origin '''+GITLAB_CICD_REPOSITORY_URL2+'''
                        git checkout main
                        git pull origin main 
                        mkdir -p '''+GITLAB_CICD_REPOSITORY_CONTEXT_DIR+'''/
                        cp ../'''+GITLAB_CODE_REPOSITORY_DIR+'''/'''+artifactPath+''' '''+GITLAB_CICD_REPOSITORY_CONTEXT_DIR+'''/
                        echo "FROM tomcat:8.5-jre8\nCOPY '''+artifactName+''' /usr/local/tomcat/webapps/\nEXPOSE 8080" > '''+GITLAB_CICD_REPOSITORY_CONTEXT_DIR+'''/Dockerfile
                        sed -i "s/'''+oldname+'''/'''+GITLAB_CODE_REPOSITORY_DIR+'''/g" '''+GITLAB_CICD_REPOSITORY_CONTEXT_DIR+'''/'''+GITLAB_CODE_REPOSITORY_DIR+'''.yaml
                        sed -i "$(grep -n 'image:' '''+GITLAB_CICD_REPOSITORY_CONTEXT_DIR+'''/'''+GITLAB_CODE_REPOSITORY_DIR+'''.yaml | grep -Eo '^[^:]+')s@$dockerognTag@'''+dockernewTag+'''@g" '''+GITLAB_CICD_REPOSITORY_CONTEXT_DIR+'''/'''+GITLAB_CODE_REPOSITORY_DIR+'''.yaml
                        echo "'''+artifactName+'''" > '''+GITLAB_CICD_REPOSITORY_CONTEXT_DIR+'''/README-ARTIFACT.md
                        echo "'''+commitInfo+'''" > '''+GITLAB_CICD_REPOSITORY_CONTEXT_DIR+'''/README.md
                        '''+webCopyCLI+'''
                        git add .
                        git config --global user.name "Jenkins Automation Server"
                        git config --global user.email "${CICD_USERNAME}@example.com"
                        git diff --quiet && git diff --staged --quiet || git commit -am "'''+commitInfo+'''"
                        git fetch --prune --prune-tags
                        git push -f '''+GITLAB_CICD_REPOSITORY_URL2+''' main 
                    '''
                    def cicdTagCnt = sh(script: 'git tag --list --sort=version:refname "*='+GITLAB_CICD_REPOSITORY_CONTEXT_DIR+'" | wc -l', returnStdout: true).trim()
                }
            }
        }
    dir(GITLAB_CICD_REPOSITORY_DIR) {
        dir(GITLAB_CICD_REPOSITORY_CONTEXT_DIR) {
            def btag = sh(script: 'cat README.md', returnStdout: true).trim()
            def rtag = btag.replace('=', '-')
            def rrtag = rtag.replace('#', '-')
            def dockernewTag = NEXUS_DOCKER_REGISTRY+"/"+projectname+"/metacicd:"+rrtag
            withCredentials([usernamePassword(credentialsId: NEXUS_CREDENTIAL_ID, usernameVariable: 'NEXUS_USER_ID', passwordVariable: 'NEXUS_USER_PASSWORD')]) {
                stage('Docker Build & Nexus push') {
				    sh '''
			            sudo docker login '''+NEXUS_DOCKER_REGISTRY+''' -u ${NEXUS_USER_ID} -p ${NEXUS_USER_PASSWORD}
                        sudo docker build -t '''+dockernewTag+''' .
                        sudo docker push '''+dockernewTag+'''
                    '''
                    sh '''
                        echo '''+dockernewTag+'''
                    '''
                }
            }
        }
    }
}
